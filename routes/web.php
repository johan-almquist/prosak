<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PagesController@home')->name('home');


Route::get('/services/subscription', 'PagesController@subscription')->name('services.subscription');
Route::get('/services/professional-protection', 'PagesController@protection')->name('services.protection');
Route::get('/contact', 'PagesController@contact')->name('contact');