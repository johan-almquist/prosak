@extends('layouts.master')


@section('title')
    Hem
@endsection


@section('css')
   <link rel="stylesheet" href="{{asset('css/slider.css')}}">
@endsection



@section('page-title')
    @include('pages.includes.home-slider')
@endsection

@section('upper-promobox')
<div class="promo promo-dark promo-full promo-uppercase bottommargin">
        <div class="container clearfix">
            <h3>VI FINNS MED ER INNAN NÅGOT HÄNDER</h3>
            <span>VI LEVER I EN VÄRLD MED ÖKAT HOT, ÄVEN I NÄRHETEN AV OSS! </span>
            <a href="#" class="button button-xlarge button-rounded">Kontakta oss idag</a>
        </div>
    </div>
@endsection
@section('content')
<div class="content-wrap">
    <div class="container clearfix">
        @include('pages.includes.services-home')
    </div>
</div>
@endsection



@section('script')
    <script src="{{asset('js/slider.js')}}"></script>

    <script>
        var tpj=jQuery;
        var revapi31;
        tpj(document).ready(function() {
            if(tpj("#rev_slider_679_1").revolution == undefined){
                revslider_showDoubleJqueryError("#rev_slider_679_1");
            }else{
                revapi31 = tpj("#rev_slider_679_1").show().revolution({
                    sliderType:"standard",
                    jsFileLocation:"include/rs-plugin/js/",
                    sliderLayout:"fullwidth",
                    dottedOverlay:"none",
                    delay:16000,
                    hideThumbs:200,
                    thumbWidth:100,
                    thumbHeight:50,
                    thumbAmount:5,
                    navigation: {
                        keyboardNavigation: "on",
                        keyboard_direction: "horizontal",
                        mouseScrollNavigation: "off",
                        onHoverStop: "off",
                        touch: {
                            touchenabled: "on",
                            swipe_threshold: 75,
                            swipe_min_touches: 1,
                            swipe_direction: "horizontal",
                            drag_block_vertical: false
                        },
                    },
                    responsiveLevels:[1140,1024,778,480],
                    visibilityLevels:[1140,1024,778,480],
                    gridwidth:[1140,1024,778,480],
                    gridheight:[700,768,960,720],
                    lazyType:"none",
                    shadow:0,
                    spinner:"off",
                    stopLoop:"off",
                    stopAfterLoops:-1,
                    stopAtSlide:-1,
                    shuffle:"off",
                    autoHeight:"off",
                    fullScreenAutoWidth:"off",
                    fullScreenAlignForce:"off",
                    fullScreenOffsetContainer: "",
                    fullScreenOffset: "0px",
                    hideThumbsOnMobile:"off",
                    hideSliderAtLimit:0,
                    hideCaptionAtLimit:0,
                    hideAllCaptionAtLilmit:0,
                    debugMode:false,
                    fallbacks: {
                        simplifyAll:"off",
                        nextSlideOnWindowFocus:"off",
                        disableFocusListener:false,
                    }
                });
            }
            revapi31.bind("revolution.slide.onloaded",function (e) {
                setTimeout( function(){ SEMICOLON.slider.sliderParallaxDimensions(); }, 200 );
            });
            revapi31.bind("revolution.slide.onchange",function (e,data) {
                SEMICOLON.slider.revolutionSliderMenu();
            });
        });	/*ready*/
    </script>
@endsection