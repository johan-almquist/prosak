@extends('layouts.master')

@section('title')
KONTAKT
@endsection

@section('upper-promobox')
<div class="promo promo-light promo-full header-stick notopborder">
    <div class="container center clearfix">
        <h3>Dygnet runt: <span><a href="tel:0104906230">010-490 62 30</a> </span></h3>
    </div>
</div>
@endsection


@section('page-title')
<section id="page-title" class="page-title-parallax page-title-dark"
    style="background-image: url('{{asset('images/about/parallax.jpg')}}'); padding: 120px 0;"
    data-bottom-top="background-position:0px 300px;" data-top-bottom="background-position:0px -300px;">

    <div class="container clearfix">
        <h1>KONTAKT</h1>
    </div>

</section>
@endsection

@section('content')
<div class="content-wrap">
    <div class="container clearfix">
        <div class="col_half">
            <div class="heading-block topmargin-sm">
                <h3>Kontaktformulär</h3>

                <form action="#" method="post">
                    <div class="form-group">
                        <label for="subject">Ämne *</label>
                        <input type="text" name="subject" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="message">Meddelande *</label>
                        <textarea name="message" rows="8" class="form-control"></textarea>
                    </div>
                    <button class="button button-3d nomargin" type="submit">Skicka</button>
                </form>
            </div>
        </div>
        <div class="col_half col_last">
            <div class="heading-block topmargin-sm">
                <h3>Kontakt uppgifter</h3>
            </div>

            <div class="feature-box fbox-center fbox-bg fbox-plain">
                <div class="fbox-icon">
                    <a href="#"><i class="icon-phone3"></i></a>
                </div>
                <h3>Anders Nygren</h3>
                <div><span class="subtitle"><a href="tel:46729761266"> +46 72 976 12 66</a></span></div>
                <div><span class="subtitle"><a href="mailto:anders.nygren@proaktivsakerhet.com">
                            anders.nygren@proaktivsakerhet.com</a></span></div>
                <h3>Karl Lundin</h3>
                <div><span class="subtitle"><a href="tel:46723172547"> +46 72 317 25 47</a></span></div>
                <div><span class="subtitle"><a href="mailto:karl.lundin@proaktivsakerhet.com">
                            karl.lundin@proaktivsakerhet.com</a></span></div>
            </div>
            <div class="feature-box fbox-center fbox-bg fbox-plain">
                <div class="fbox-icon">
                    <a href="#"><i class="icon-map-marker2"></i></a>
                </div>
                <h3>Huvudkontor</h3>
                <address>
                    Proaktiv Säkerhet<br>
                    Knarrarnäsgatan 15<br>
                    164 40 Kista
                </address>
            </div>
        </div>
    </div>
</div>
@endsection