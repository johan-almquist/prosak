@extends('layouts.master')
@section('title')
    ABONNEMANGS SÄKERHET
@endsection
@section('page-title')
<section id="page-title" class="page-title-parallax page-title-dark"
    style="background-image: url('{{asset('images/about/parallax.jpg')}}'); padding: 120px 0;"
    data-bottom-top="background-position:0px 300px;" data-top-bottom="background-position:0px -300px;">

    <div class="container clearfix">
        <h1>Abonnemangs säkerhet</h1>
        <span>VI HJÄLPER ER! NU OCH SÅ LÄNGE SOM NI BEHÖVER OSS.</span>
    </div>

</section>
@endsection


@section('content')
<div class="container clearfix">

    <div class="col_two_fifth topmargin nobottommargin">
            <img src="https://via.placeholder.com/500" alt="" class="p-2">
    </div>

    <div class="col_three_fifth nobottommargin col_last">

        <div class="heading-block">
            <h3>ABONNEMANG INOM SÄKERHET</h3>
            <span>MÅNGA FÖRETAG HAR INTE BEHOV AV ATT HA EGEN SÄKERHETSCHEF ELLER KUNNIG PERSONAL INOM SÄKERHET. ALLT FÖR OFTA SÅ DELEGERAS DETTA TILL PERSONAL SOM INTE HAR DEN KOMPETENS INOM SÄKERHET SOM ÖNSKAS.</span>
        </div>

        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vero quod consequuntur quibusdam, enim expedita sed
            quia nesciunt incidunt accusamus necessitatibus modi adipisci officia libero accusantium esse hic,
            obcaecati, ullam, laboriosam!</p>

        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corrupti vero, animi suscipit id facere officia.
            Aspernatur, quo, quos nisi dolorum aperiam fugiat deserunt velit rerum laudantium cum magnam excepturi quod,
            fuga architecto provident, cupiditate delectus voluptate eaque! Sit neque ut eum, voluptatibus odit cum
            dolorum ipsa voluptates inventore cumque a.</p>

        <a href="#">Learn more →</a>

    </div>

    <div class="clear"></div>
    <div class="line"></div>

    <div class="col_three_fifth">

        <div class="heading-block">
            <h3>Abonnemangs säkerhet</h3>
            <span>
                <p>Abonnemanget ger er många fördelar. Vi finns som er kontakt inom säkerhet från dag 1 och fram tills ni inte behöver oss!</p> 
                <p>Abonnemanget är en kostnadsfri tjänst som löper på årsvis, mot att ni använder Proaktiv Säkerhet till all säkerhet.</p> 
                <p>Vi är så säkra att ni är nöjda med oss att ni, helst, kan avbryta vårt samarbete som om ni är missnöjda med oss efter avslutad installation eller annatkonkret jobb.</p> 
            </span>
        </div>

        <p>
            <strong>Abonnemanget ger er bland annat:</strong>
        </p>

        <ul class="list-group">
                <li class="list-group-item">Egen säkerhetssamordnare till er organisation</li>
                <li class="list-group-item">Direktnummer till er säkerhetssamordnare</li>
                <li class="list-group-item">Nyckel/passagebehörigheter</li>
                <li class="list-group-item">Kostnadsfri support per telefon inom dagliga säkerhetsfrågor</li>
                <li class="list-group-item">Rabatterade priser på konsulttjänster. Från 650 kr/h exkl. moms</li>
                <li class="list-group-item">Enklare riskbedömning</li>
                <li class="list-group-item">Fri upphandling av säkerhetsprodukter och tjänster</li>
                <li class="list-group-item">och mycket mer</li>
            </ul>

    </div>

    <div class="col_two_fifth topmargin col_last">
        <img src="https://via.placeholder.com/500" alt="">
    </div>

</div>
@endsection