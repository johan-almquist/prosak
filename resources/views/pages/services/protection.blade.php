@extends('layouts.master')
@section('title')
Professionel säkerhet
@endsection

@section('page-title')
<section id="page-title" class="page-title-parallax page-title-dark"
    style="background-image: url('{{asset('images/about/parallax.jpg')}}'); padding: 120px 0;"
    data-bottom-top="background-position:0px 300px;" data-top-bottom="background-position:0px -300px;">

    <div class="container clearfix">
        <h1>Professionel säkerhet</h1>
    </div>

</section>
@endsection

@section('content')

<section id="content" style="margin-bottom: 0px;">
    <div class="container center clearfix">
        <div class="heading-block center">
            <h1>PERSONELLT SKYDD OCH SERVICETJÄNSTER</h1>
            <span>PROAKTIV SÄKERHET ARBETAR HÅRT FÖR ATT GE ER RÄTT PERSONELLT SKYDD I FORM AV BEVAKNINGSPERSONAL,
                PERSONSKYDD, RECEPTIONISTER OCH HOTELLPORTIER</span>
        </div>
    </div>



</section>
<section id="content">
    <div class="content-wrap">
        <div class="container clearfix">
            <div class="row">
                <div class="col-md-4 mb-5" data-animate="fadeIn">
                    <div class="feature-box media-box">
                        <div class="fbox-media">
                            <img src="https://via.placeholder.com/236x177" alt="">
                        </div>
                        <div class="fbox-desc">
                            <h3>Ronderande väktare</h3>
                            <p>
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Fugit veniam eveniet, rerum
                                ducimus quae est, blanditiis sequi voluptates debitis quos quia ipsam sint perferendis
                                veritatis non. Autem neque dolore non!
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 mb-5" data-animate="fadeIn">
                    <div class="feature-box media-box">
                        <div class="fbox-media">
                            <img src="https://via.placeholder.com/236x177" alt="">
                        </div>
                        <div class="fbox-desc">
                            <h3>Stationära väktare</h3>
                            <p>
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Fugit veniam eveniet, rerum
                                ducimus quae est, blanditiis sequi voluptates debitis quos quia ipsam sint perferendis
                                veritatis non. Autem neque dolore non!
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 mb-5" data-animate="fadeIn">
                    <div class="feature-box media-box">
                        <div class="fbox-media">
                            <img src="https://via.placeholder.com/236x177" alt="">
                        </div>
                        <div class="fbox-desc">
                            <h3>Ordningsvakter</h3>
                            <p>
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Fugit veniam eveniet, rerum
                                ducimus quae est, blanditiis sequi voluptates debitis quos quia ipsam sint
                                perferendis
                                veritatis non. Autem neque dolore non!
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 mb-5" data-animate="fadeIn">
                    <div class="feature-box media-box">
                        <div class="fbox-media">
                            <img src="https://via.placeholder.com/236x177" alt="">
                        </div>
                        <div class="fbox-desc">
                            <h3>Hotellportier</h3>
                            <p>
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Fugit veniam eveniet, rerum
                                ducimus quae est, blanditiis sequi voluptates debitis quos quia ipsam sint
                                perferendis
                                veritatis non. Autem neque dolore non!
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 mb-5" data-animate="fadeIn">
                    <div class="feature-box media-box">
                        <div class="fbox-media">
                            <img src="https://via.placeholder.com/236x177" alt="">
                        </div>
                        <div class="fbox-desc">
                            <h3>Personskydd</h3>
                            <p>
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Fugit veniam eveniet, rerum
                                ducimus quae est, blanditiis sequi voluptates debitis quos quia ipsam sint
                                perferendis
                                veritatis non. Autem neque dolore non!
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection