<section id="slider" class="slider-element revslider-wrap">
        <div class="slider-parallax-inner">
    
            <!--
            #################################
                - THEMEPUNCH BANNER -
            #################################
            -->
            <div id="rev_slider_679_1_wrapper" class="rev_slider_wrapper fullwidth-container"  style="padding:0px;">
                <!-- START REVOLUTION SLIDER 5.1.4 fullwidth mode -->
                <div id="rev_slider_679_1" class="rev_slider fullwidthbanner" style="display:none;" data-version="5.1.4">
                    <ul>    <!-- SLIDE  -->
                        <li class="dark" data-transition="fade" data-slotamount="1" data-masterspeed="1500" data-thumb="images/slider/rev/ken-1-thumb.jpg" data-delay="10000"  data-saveperformance="off" data-title="Responsive Design">
                            <!-- MAIN IMAGE -->
                            <img src="https://via.placeholder.com/4184x2307"  alt="kenburns1" data-bgposition="center bottom" data-bgpositionend="center top" data-kenburns="off" data-duration="10000" data-ease="Linear.easeNone" data-scalestart="100" data-scaleend="120" data-rotatestart="0" data-rotateend="0" data-blurstart="0" data-blurend="0" data-offsetstart="0 0" data-offsetend="0 0" class="rev-slidebg" data-no-retina>
                            <!-- LAYERS -->
                            <!-- LAYER NR. 2 -->
                            <div class="tp-caption customin ltl tp-resizeme revo-slider-emphasis-text nopadding noborder "
                                 data-x="middle" data-hoffset="0"
                                 data-y="top" data-voffset="230"
                                 data-fontsize="['60','50','40','34']"
                                 data-transform_in="x:0;y:150;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;s:800;e:Power4.easeOutQuad;"
                                 data-speed="800"
                                 data-start="1200"
                                 data-easing="easeOutQuad"
                                 data-splitin="none"
                                 data-splitout="none"
                                 data-elementdelay="0.01"
                                 data-endelementdelay="0.1"
                                 data-endspeed="1000"
                                 data-endeasing="Power4.easeIn" style="z-index: 3; white-space: nowrap;">VI SER TILL ATT NI FÅR RÄTT SÄKERHET
                            </div>
    
                            <div class="tp-caption customin ltl tp-resizeme revo-slider-caps-text uppercase"
                                 data-x="middle" data-hoffset="0"
                                 data-y="top" data-voffset="325"
                                 data-transform_in="x:0;y:150;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;s:800;e:Power4.easeOutQuad;"
                                 data-speed="800"
                                 data-start="1000"
                                 data-easing="easeOutQuad"
                                 data-splitin="none"
                                 data-splitout="none"
                                 data-elementdelay="0.01"
                                 data-endelementdelay="0.1"
                                 data-endspeed="1000"
                                 data-fontsize="['25']"
                                 data-endeasing="Power4.easeIn" style="z-index: 3; white-space: nowrap;">Proaktiv Säkerhet är er totalleverantör av fysiskt, tekniskt och personellt skydd</div>
                        </li>
                    </ul>
                </div>
            </div><!-- END REVOLUTION SLIDER -->
    
        </div>
    </section>