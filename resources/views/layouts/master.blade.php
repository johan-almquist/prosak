<!DOCTYPE html>
<html dir="ltr" lang="en-US">

<head>

	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<!-- Stylesheets
	============================================= -->
	<link href="{{asset('css/canvas.css')}}" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="{{asset('css/icons.css')}}">	
	@yield('css')

	<!-- Document Title
	============================================= -->
	<title>@yield('title') | Canvas</title>

</head>

<body class="no-transition stretched">

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">

		<header id="header" class="full-header">
			<div id="header-wrap">
				<div class="container clearfix">
					<div id="primary-menu-trigger">
						<i class="icon-reorder"></i>
					</div>
					<!-- Logo ============================================= -->
					<div id="logo">
						<a href="/" class="standard-logo">
							<img src="{{asset('images/logo.png')}}" alt="EFS logo">
						</a>
						<a href="/" class="retina-logo"">
                                    <img src=" {{asset('images/logo.png')}}" alt="EFS logo">
						</a>
					</div>
					<!-- Primary Navigation ============================================= -->
					@include('layouts.partials._nav')

				</div>
			</div>
		</header>

		<!-- Page Title
		============================================= -->
		@yield('page-title')

		@yield('upper-promobox')

		<!-- Content
		============================================= -->
		<section id="content">

			<div class="content-wrap">
				@yield('content')
			</div>

		</section><!-- #content end -->

		<footer id="footer" class="dark">

				<div class="container">
		
					@include('layouts.partials.footer')
		
				</div>

				<!-- Copyrights ============================================= -->
				<div id="copyrights">

						<div class="container clearfix">
			
							&copy; PROAKTIV SÄKERHET 2019
							<div class="alignright">
									FÖRETAGET INNEHAR F-SKATT OCH ÄR MOMSREGISTRERAT
							</div>
			
						</div>
			
					</div>
			

	</div><!-- #wrapper end -->

	<!-- Go To Top
	============================================= -->
	<div id="gotoTop" class="icon-angle-up"></div>

	<!-- JavaScripts
	============================================= -->
	<script src="{{asset('js/canvas.js')}}"></script>
	@yield('script')
</body>

</html>