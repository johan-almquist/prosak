<div class="footer-widgets-wrap clearfix">
        <div class="col_half">
            <div class="widget">
                <div style="background: url('{{asset('images/world-map.png')}}') no-repeat center center; background-size: 100%;">
                    <address>
                        <strong>Huvudkontor</strong><br>
                        Proaktiv Säkerhet<br>
                        Knarrarnäsgatan 15<br>
                        164 40 Kista
                    </address>
                    <abbr title="Felanmälan"><strong>ANDERS NYGREN</strong></abbr><a href="tel:+46729761266"> +46 72 976 12 66</a> <br>
                    <abbr title="Kundtjänst"><strong>KARL LUNDIN</strong></abbr><a href="tel:087606131"> +46 72 317 25 47</a> <br>
                    <abbr title="E-post"><strong>E-post</strong></abbr> <a href="mailto:kundtjanst@efsab.se">mail@proaktivsakerhet.com</a>
                </div>
            </div>
        </div>
        <div class="col_half alignleft col_last">
            <div class="widget">
            </div>
        </div>
        <div class="col_one_third">
    
        </div>
    </div>