<nav id="primary-menu" class="">
    <!-- Left side menu -->
    <ul>
        <li>
            <a href="/">
                <div>HEM</div>
            </a>
        </li>
        <li class="mega-menu"> <a href="#">
                <div>Våra tjänster</div>
            </a>
            <div class="mega-menu-content style-2 clearfix">
                <ul class="mega-menu-column col-md-3">
                    <li>
                    <a href="{{route('services.subscription')}}">
                            <div>ABONNEMANGS SÄKERHET</div>
                        </a>
                    </li>
                </ul>
                <ul class="mega-menu-column col-md-3">
                    <li>
                        <a href="/">
                            <div>FYSISKT OCH TEKNISKT SKYDD </div>
                        </a>
                    </li>
                </ul>
                <ul class="mega-menu-column col-md-3">
                    <li>
                    <a href="{{route('services.protection')}}">
                            <div>PERSONELLT SKYDD </div>
                        </a>
                    </li>
                </ul>
                <ul class="mega-menu-column col-md-3">
                    <li>
                        <a href="/">
                            <div>KONSULTTJÄNSTER</div>
                        </a>
                    </li>
                </ul>
            </div>
        </li>

        <li>
        <a href="{{route('contact')}}">
                <div>KONTAKT</div>
            </a>
        </li>
    </ul>
</nav>