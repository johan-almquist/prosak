<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function home() {
        return view('pages.home');
    }

    public function subscription() {
        return view('pages.services.subscription');
    }

    public function protection(){
        return view('pages.services.protection');
    }

    public function contact() {
        return view('pages.contact');
    }
}
