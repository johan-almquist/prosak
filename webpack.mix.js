const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// Canvas core
mix.styles([
'public/vendor/canvas/css/bootstrap.css',
   'public/vendor/canvas/style.css',
   'public/vendor/canvas/css/responsive.css',
   'public/vendor/canvas/css/dark.css',
   'public/vendor/canvas/css/swiper.css',
   'public/vendor/canvas/css/animate.css',
], 'public/css/canvas.css');

mix.scripts([
    'public/vendor/canvas/js/jquery.js',
    'public/vendor/canvas/js/plugins.js',
    'public/vendor/canvas/js/functions.js',
], 'public/js/canvas.js');


// Icons
mix.styles([
    'public/vendor/canvas/css/font-icons.css',
], 'public/css/icons.css');

//slider
mix.styles([
    'public/vendor/canvas/include/rs-plugin/css/settings.css',
    'public/vendor/canvas/include/rs-plugin/css/layers.css',
    'public/vendor/canvas/include/rs-plugin/css/navigation.css',
], 'public/css/slider.css');

mix.scripts([
    'public/vendor/canvas/include/rs-plugin/js/jquery.themepunch.tools.min.js',
    'public/vendor/canvas/include/rs-plugin/js/jquery.themepunch.revolution.min.js',
    'public/vendor/canvas/include/rs-plugin/js/extensions/revolution.extension.video.min.js',
    'public/vendor/canvas/include/rs-plugin/js/extensions/revolution.extension.slideanims.min.js',
    'public/vendor/canvas/include/rs-plugin/js/extensions/revolution.extension.actions.min.js',
    'public/vendor/canvas/include/rs-plugin/js/extensions/revolution.extension.layeranimation.min.js',
    'public/vendor/canvas/include/rs-plugin/js/extensions/revolution.extension.kenburn.min.js',
    'public/vendor/canvas/include/rs-plugin/js/extensions/revolution.extension.navigation.min.js',
    'public/vendor/canvas/include/rs-plugin/js/extensions/revolution.extension.migration.min.js',
    'public/vendor/canvas/include/rs-plugin/js/extensions/revolution.extension.parallax.min.js',
 ], 'public/js/slider.js');